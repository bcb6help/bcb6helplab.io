<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Notifying a wizard of IDE events</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0> DevGuide: <A HREF="bcb61ywj.htm">Component writer's guide</A></B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="IDE, responding to,">
	<PARAM NAME="Keyword" VALUE="Tools API, responding to IDE events,">
</OBJECT>

<H2><A NAME="cwgnotifyingawizardofideevents"></A>Notifying a wizard of IDE events</H2>

<P><A HREF="bcb63pb5.htm">Topic groups</A>See also</P>

<P>An important aspect of writing a well-behaved wizard is to have the wizard respond to IDE events. In particular, any wizard that keeps track of module interfaces must know when the user closes the module, so the wizard can release the interface. To do this, the wizard needs a notifier, which means you must write a notifier class.</P>

<P>All notifier classes implement one or more notifier interfaces. The notifier interfaces define callback methods; the wizard registers a notifier object with the Tools API, and the IDE calls back to the notifier when something important happens.</P>

<P>Every notifier interface inherits from <I>IOTANotifier</I>, although not all of its methods are used for a particular notifier. The following table lists all the notifier interfaces, and gives a brief description of each one.</P>

<TABLE cols=2 width=657>

<TR VALIGN="top">
<TD width=33%><B>Interface</B></TD>
<TD width=67%><B>Description</B></TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTANotifier</TD>
<TD width=67%>Abstract base class for all notifiers</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTABreakpointNotifier</TD>
<TD width=67%>Triggering or changing a breakpoint in the debugger</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTADebuggerNotifier</TD>
<TD width=67%>Running a program in the debugger, or adding or deleting breakpoints</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAEditLineNotifier</TD>
<TD width=67%>Tracking movements of lines in the source editor</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAEditorNotifier</TD>
<TD width=67%>Modifying or saving a source file, or switching files in the editor</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAFormNotifier</TD>
<TD width=67%>Saving a form, or modifying the form or any components on the form (or data module)</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAIDENotifier</TD>
<TD width=67%>Loading projects, installing packages, and other global IDE events</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAMessageNotifier</TD>
<TD width=67%>Adding or removing tabs (message groups) in the message view</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAModuleNotifier</TD>
<TD width=67%>Changing, saving, or renaming a module</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAProcessModNotifier</TD>
<TD width=67%>Loading a process module in the debugger?</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAProcessNotifier</TD>
<TD width=67%>Creating or destroying threads and processes in the debugger</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAThreadNotifier</TD>
<TD width=67%>Changing a thread�s state in the debugger</TD>
</TR>

<TR VALIGN="top">
<TD width=33%>IOTAToolsFilterNotifier</TD>
<TD width=67%>Invoking a tools filter</TD>
</TR>
</TABLE><BR>

<P>To see how to use notifiers, consider the example in <A HREF="bcb60lir.htm">Creating forms and projects</A>. Using module creators, the example creates a wizard that adds a comment to each source file. The comment includes the unit�s initial name, but the user almost always saves the file under a different name. In that case, it would be a courtesy to the user if the wizard updated the comment to match the file�s true name.</P>

<P>To do this, you need a module notifier. The wizard saves the module interface that <I>CreateModule</I> returns, and uses it to register a module notifier. The module notifier receives notification when the user modifies the file or saves the file, but these events are not important for this wizard, so the <I>AfterSave</I> and related functions all have empty bodies. The important function is <I>ModuleRenamed</I>, which the IDE calls when the user saves the file under a new name. The declaration for the module notifier class is shown below:</P>

<P><B>class</B> ModuleNotifier : <B>public</B> NotifierObject, <B>public</B> IOTAModuleNotifier</P>

<P>{</P>

<P><B>typedef</B> NotifierObject inherited;</P>

<P><B>public</B>:</P>

<P><B>__fastcall</B> ModuleNotifier(<B>const</B> _di_IOTAModule module);</P>

<P><B>__fastcall</B> ~ModuleNotifier();</P>

<P>// IOTAModuleNotifier</P>

<P><B>virtual bool __fastcall</B>CheckOverwrite();</P>

<P><B>virtual void __fastcall</B>ModuleRenamed(<B>const</B> AnsiString NewName);</P>

<P>// IOTANotifier</P>

<P><B>void __fastcall</B> AfterSave();</P>

<P><B>void __fastcall</B>BeforeSave();</P>

<P><B>void __fastcall</B> Destroyed();</P>

<P><B>void __fastcall</B> Modified();</P>

<P><B>protected</B>:</P>

<P>// IInterface</P>

<P><B>virtual</B> HRESULT <B>__stdcall</B> QueryInterface(<B>const</B> GUID&amp;, <B>void</B>**);</P>

<P><B>virtual</B> ULONG <B>__stdcall</B> AddRef();</P>

<P><B>virtual</B> ULONG <B>__stdcall</B> Release();</P>

<P><B>private</B>:</P>

<P>_di_IOTAModule module;</P>

<P>AnsiString name;        // Remember the module's old name.</P>

<P><B>int</B> index;              // Notifier index.</P>

<P>};</P>

<P>One way to write a notifier is to have it register itself automatically in its constructor. The destructor unregisters the notifier. In the case of a module notifier, the IDE calls the <I>Destroyed</I> method when the user closes the file. In that case, the notifier must unregister itself and release its reference to the module interface. The IDE releases its reference to the notifier, which reduces its reference count to zero and frees the object. Therefore, you need to write the destructor defensively: the notifier might already be unregistered.</P>

<P><B>__fastcall</B> ModuleNotifier::ModuleNotifier(<B>const</B> _di_IOTAModule module)</P>

<P>: index(-1), module(module)</P>

<P>{</P>

<P>// Register this notifier.</P>

<P>index = module-&gt;AddNotifier(this);</P>

<P>// Remember the module's old name.</P>

<P>name = ChangeFileExt(ExtractFileName(module-&gt;FileName), "");</P>

<P>}</P>

<P><B>__fastcall</B> ModuleNotifier::~ModuleNotifier()</P>

<P>{</P>

<P>// Unregister the notifier if that hasn't happened already.</P>

<P><B>if</B> (index &gt;= 0)</P>

<P>module-&gt;RemoveNotifier(index);</P>

<P>}</P>

<P><B>void __fastcall</B>ModuleNotifier::Destroyed()</P>

<P>{</P>

<P>// The module interface is being destroyed, so clean up the notifier.</P>

<P><B>if</B> (index &gt;= 0)</P>

<P>{</P>

<P>// Unregister the notifier.</P>

<P>module-&gt;RemoveNotifier(index);</P>

<P>index = -1;</P>

<P>}</P>

<P>module = 0;</P>

<P>}</P>

<P>The IDE calls back to the notifier�s <I>ModuleRenamed</I> function when the user renames the file. The function takes the new name as a parameter, which the wizard uses to update the comment in the file. To edit the source buffer, the wizard uses an edit position interface. The wizard finds the right position, double checks that it found the right text, and replaces that text with the new name.</P>

<P><B>void __fastcall</B> ModuleNotifier::ModuleRenamed(<B>const</B> AnsiString NewName)</P>

<P>{</P>

<P>// Get the module name from the new file name.</P>

<P>AnsiString ModuleName = ChangeFileExt(ExtractFileName(NewName), "");</P>

<P><B>for</B> (<B>int</B> i = 0; i &lt; module-&gt;GetModuleFileCount(); ++i)</P>

<P>{</P>

<P>// Update every source editor buffer.</P>

<P>_di_IOTAEditor editor = module-&gt;GetModuleFileEditor(i);</P>

<P>_di_IOTAEditBuffer buffer;</P>

<P><B>if</B> (editor-&gt;Supports(buffer))</P>

<P>{</P>

<P>_di_IOTAEditPosition pos = buffer-&gt;GetEditPosition();</P>

<P>// The module name is on line 2 of the comment.</P>

<P>// Skip leading white space and copy the old module name,</P>

<P>// to double check we have the right spot. </P>

<P>pos-&gt;Move(2, 1);</P>

<P>pos-&gt;MoveCursor(mmSkipWhite | mmSkipRight);</P>

<P>AnsiString check = pos-&gt;RipText("", rfIncludeNumericChars | rfIncludeAlphaChars);</P>

<P><B>if</B> (check == name)</P>

<P>{</P>

<P>pos-&gt;Delete(check.Length());    // Delete the old name.</P>

<P>pos-&gt;InsertText(ModuleName);    // Insert the new name.</P>

<P>name = ModuleName;              // Remember the new name.</P>

<P>}</P>

<P>}</P>

<P>}</P>

<P>}</P>

<P>What if the user inserts additional comments above the module name? In that case, you need to use an edit line notifier to keep track of the line number where the module name sits. To do this, use the <I>IOTAEditLineNotifier</I> and <I>IOTAEditLineTracker</I> interfaces.</P>

<P>You need to be cautious when writing notifiers. You must make sure that no notifier outlives its wizard. For example, if the user were to use the wizard to create a new unit, then unload the wizard, there would still be a notifier attached to the unit. The results would be unpredictable, but most likely, the IDE would crash. Thus, the wizard needs to keep track of all of its notifiers, and must unregister every notifier before the wizard is destroyed. On the other hand, if the user closes the file first, the module notifier receives a <I>Destroyed</I> notification, which means the notifier must unregister itself and release all references to the module. The notifier must remove itself from the wizard�s master notifier list, too.</P>

<P>Below is the final version of the wizard�s <I>Execute</I> function. It creates the new module, uses the module interface and creates a module notifier, then saves the module notifier in an interface list (<I>TInterfaceList</I>).</P>

<P><B>void __fastcall</B> DocWizard::Execute()</P>

<P>{</P>

<P>_di_IOTAModuleServices svc;</P>

<P>BorlandIDEServices-&gt;Supports(svc);</P>

<P>_di_IOTAModule module = svc-&gt;CreateModule(<B>new</B> Creator(creator_type));</P>

<P>_di_IOTAModuleNotifier notifier = <B>new</B> ModuleNotifier(module);</P>

<P>list-&gt;Add(notifier);</P>

<P>}</P>

<P>The wizard�s destructor iterates over the interface list and unregisters every notifier in the list. Simply letting the interface list release the interfaces it holds is not sufficient because the IDE also holds the same interfaces. You must tell the IDE to release the notifier interfaces in order to free the notifier objects. In this case, the destructor tricks the notifiers into thinking their modules have been destroyed. In a more complicated situation, you might find it best to write a separate Unregister function for the notifier class.</P>

<P><B>__fastcall</B>DocWizard::~DocWizard()</P>

<P>{</P>

<P>// Unregister all the notifiers in the list.</P>

<P><B>for</B> (<B>int</B> i = list-&gt;Count; --i &gt;= 0; )</P>

<P>{</P>

<P>_di_IOTANotifier notifier;</P>

<P>list-&gt;Items[i]-&gt;Supports(notifier);</P>

<P>// Pretend the associated object has been destroyed.</P>

<P>// That convinces the notifier to clean itself up.</P>

<P>notifier-&gt;Destroyed();</P>

<P>list-&gt;Delete(i);</P>

<P>}</P>

<P><B>delete</B> list;</P>

<P><B>delete</B> item;</P>

<P>}</P>

<P>The rest of the wizard manages the mundane details of registering the wizard, installing menu items, and the like. <A HREF="bcb699ik.htm">Installing a wizard DLL</A>expands on the details of registering a wizard by discussing how to do so in a DLL instead of a package.</P>

</BODY>
</HTML>
