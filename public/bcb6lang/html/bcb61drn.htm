<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Bit fields</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="bit fields">
</OBJECT>

<P><A NAME="bitfields"></A><B>Bit fields</B></P>

<P>See also</P>

<P>Bit fields are specified numbers of bits that may or may not have an associated identifier. Bit fields offer a way of subdividing structures (structs, unions, classes) into named parts of user-defined sizes.</P>

<P><B>Declaring bit fields</B></P>

<P>You specify the bit-field width and optional identifier as follows:</P>

<P><I>type-specifier &lt;bitfield-id&gt; : width;</I></P>

<P>In C++, <I>type-specifier</I> is <B>bool</B>, <B>char</B>, <B>unsigned char</B>, <B>short</B>, <B>unsigned short</B>, <B>long</B>, <B>unsigned long</B>, <B>int</B>, <B>unsigned int, __int64</B> or <B>unsigned __int64</B>. In strict ANSI C, <I>type-specifier</I> is <B>int</B> or <B>unsigned int</B>.</P>

<P>The expression <I>width</I> must be present and must evaluate to a constant integer. In C++ , the width of a bit field may be declared of any size. In strict ANSI C, the width of a bit field may be declared only up to the size of the declared type. A zero length bit field skips to the next allocation unit.</P>

<P>If the bit field identifier is omitted, the number of bits specified in <I>width</I> is allocated, but the field is not accessible. This lets you match bit patterns in, say, hardware registers where some bits are unused.</P>

<P>Bit fields can be declared only in structures, unions, and classes. They are accessed with the same member selectors ( . and -&gt;) used for non-bit-field members.</P>

<P><B>Limitations of using bit fields</B></P>

<P>When using bit fields, be aware of the following issues:</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>The code will be non-portable since the organization of bits-within-bytes and bytes-within-words is machine dependent.</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>You cannot take the address of a bit field; so the expression <I>&amp;mystruct.x</I> is illegal if <I>x</I> is a bit field identifier, because there is no guarantee that <I>mystruct.x</I> lies at a byte address.</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>Bit fields are used to pack more variables into a smaller data space, but causes the compiler to generate additional code to manipulate these variables. This costs in terms of code size and execution time.</P>

<P>Because of these disadvantages, using bit fields is generally discouraged, except for certain low-level programming. A recommended alternative to having one-bit variables, or flags, is to use defines. For example:</P>

<P>#define Nothing0x00</P>

<P>#define bitOne0x01</P>

<P>#define bitTwo0x02</P>

<P>#define bitThree0x04</P>

<P>#define bitFour0x08</P>

<P>#define bitFive0x10</P>

<P>#define bitSix0x20</P>

<P>#define bitSeven0x40</P>

<P>#define bitEight0x80</P>

<P>can be used to write code like:</P>

<P>if (flags &amp; bitOne) {...}    // is bit One turned on</P>

<P>flags |= bitTwo;               // turn bit Two on</P>

<P>flags &amp;= ~bitThree;         // turn bit Three off</P>

<P>Similar schemes can be made for bit fields of any size.</P>

<P><B>Padding of bit fields</B></P>

<P>In C++ , if the width size is larger than the type of the bit field, the compiler will insert padding equal to the requested width size minus the size of the type of the bit field. So, declaration:</P>

<P>struct mystruct</P>

<P>{</P>

<P>int i : 40;</P>

<P>int j : 8;</P>

<P>};</P>

<P>will create a 32 bit storage for 'i', an 8 bit padding, and 8 bit storage for 'j'. To optimize access, the compiler will consider 'i' to be a regular int variable, not a bit field.</P>

<P><B>Layout and alignment</B></P>

<P>Bit fields are broken up into groups of consecutive bit fields of the same type, without regard to signedness. Each group of bit fields is aligned to the current alignment of the type of the members of the group. This alignment is determined by the type AND by the setting of the overall alignment (set by the byte alignment option aN). Within each group, the compiler will pack the bit fields inside of areas as large as the type size of the bit fields. However, no bit field is allowed to straddle the boundary between 2 of those areas. The size of the total structure will be aligned, as determined by the current alignment.</P>

<P><B>Example of bit field layout, padding, and alignment</B></P>

<P>In the following C++ declaration, <I>my_struct</I> contains 6 bit fields of 3 different types, <B>int</B>, <B>long</B>, and <B>char</B>:</P>

<P>struct my_struct</P>

<P>{</P>

<P>int one : 8;</P>

<P>unsigned int two : 16;</P>

<P>unsigned long three : 8;</P>

<P>long four : 16;</P>

<P>long five : 16;</P>

<P>char six : 4;</P>

<P>};</P>

<P>Bit fields 'one' and 'two' will be packed into one 32-bit area.</P>

<P>Next, the compiler inserts padding, if necessary, based on the current alignment, and the type of <I>three</I>, because the type changes between the declarations of variables <I>two</I> and <I>three</I>. For example, if the current alignment is byte alignment (-a1), no padding is needed; whereas, if the alignment is 4 bytes (-a4), then 8-bit padding is inserted.</P>

<P>Next, variables <I>three</I>, <I>four</I>, and <I>five</I> are all of type <B>long</B>. Variables <I>three</I> and <I>four</I> are packed into one 32 bit area, but <I>five</I> can not be packed into that same area, since that would create an area of 40 bits, which is more than the 32 bit allowed for the <B>long</B> type. To start a new area for <I>five</I>, the compiler would insert no padding if the current alignment is byte alignment, or would insert 8 bits of padding if the current alignment is dword (4 byte) alignment.</P>

<P>With variable <I>six</I>, the type changes again. Since <B>char</B> is always byte aligned, no padding is needed. To force alignment for the whole struct, the compiler will finish up the last area with 4 bits of padding if byte alignment is used, or 12 bits of padding if dword alignment is used.</P>

<P>The total size of my_struct is 9 bytes with byte alignment, or 12 bytes with dword alignment.</P>

<P>To get the best results when using bit fields you should:</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>Sort bit fields by type</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>Make sure they are packed inside the areas by ordering them such that no bit field will straddle an area boundary</P>

<P><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0>Make sure the struct is filled as much as possible.</P>

<P>Another recommendation is to force byte alignment for this struct, by emitting "#pragma option -a1". If you want to know how big your struct is, follow it by "#pragma sizeof(mystruct)", which  gives you the size.</P>

<P><B>Using one bit signed fields</B></P>

<P>For a signed type of one bit, the possible values are 0 or 1. For an unsigned type of one bit, the possible values are 0 or 1. Note that if you assign 1 to a signed bit field, the value will be evaluated as 1 (negative one).</P>

<P>When storing the values <B>true</B> and <B>false</B> into a one bit sized bit field of a signed type, you can not test for equality to <B>true</B> because signed one bit sized bit fields can only hold the values '0' and '-1', which are not compatible with <B>true</B> and <B>false</B>. You can, however, test for non-zero.</P>

<P>For unsigned varieties of all types, and of course for the <B>bool</B> type, testing for equality to <B>true</B> will work as expected.</P>

<P>Thus:</P>

<P>struct mystruct</P>

<P>{</P>

<P>int flag : 1;</P>

<P>} M;</P>

<P>int testing() { M.flag = true; if (M.flag == true) printf("success");}</P>

<P>will NOT work, but:</P>

<P>struct mystruct</P>

<P>{</P>

<P>int flag : 1;</P>

<P>} M;</P>

<P>int testing() { M.flag = true; if (M.flag) printf("success");}</P>

<P>works just fine.</P>

<P><B>Notes on compatibility</B></P>

<P>Between versions of the compiler, changes can be made to default alignment, or for purposes of compatibility with other compilers. Consequently, this could change the alignment of bit fields. Therefore, there is no guarantee that the alignment of bit fields will be consistent between versions of the C++Builder compiler. To check the backward compatibility of bit fields in your code you can add an assert that checks for the structure size you are expecting.</P>

<P>According to the C and C++ language specifications, the alignment and storage of bit fields is implementation defined. Therefore, compilers can align and store bit fields differently. If you want complete control over the layout of bit fields, it is advisable to write your own bit field accessing routines and create your own bit fields.</P>

</BODY>
</HTML>
