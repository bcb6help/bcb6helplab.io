<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Common uses of the C locale</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0> Standard C++ Library: Locales and Iostreams Guide</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="localization,C locale">
</OBJECT>

<H1><A NAME="common uses of the c locale"></A>Common uses of the C locale</H1>

<P>The C locale is commonly used as a default locale, a native locale, or in multiple locale applications.</P>

<P><B>Default locale. </B>As a developer, you may never require internationalization features, and thus you may never call setlocale(). If you can safely assume that users of your applications are accommodated by the classic US English ASCII behavior, you have no need for localization. Without even knowing it, you will always use the default locale, which is the US English ASCII locale.</P>

<P><B>Native locale. </B>If you do plan on localizing your program, the appropriate strategy may be to retrieve the native locale once at the beginning of your program, and never, ever change this setting again. This way your application will adapt itself to one particular locale, and use this throughout its entire runtime. Users of such applications can explicitly set their favorite locale before starting the application. On Unix systems, they do this by setting environment variables such as LANG; other operating systems may use other methods.</P>

<P>In your program, you can specify that you want to use the user�s preferred native locale by calling setlocale("") at startup, passing an empty string as the locale name. The empty string tells setlocale to use the locale specified by the user in the environment. </P>

<P><B>Multiple locales. </B>It may well happen that you do have to work with multiple locales. For example, to implement an application for Switzerland, you might want to output messages in Italian, French, and German. As the C locale is a global data structure, you will have to switch locales several times.</P>

<P>Let's look at an example of an application that works with multiple locales. Imagine an application that prints invoices to be sent to customers all over the world. Of course, the invoices must be printed in the customer's native language, so the application must write output in multiple languages. Prices to be included in the invoice are taken from a single price list. If we assume the application is used by a US company, the price list will be in US English.</P>

<P>The application reads input (the product price list) in US English, and writes output (the invoice) in the customer's native language, say German. Since there is only one global locale in C that affects both input and output, the global locale must change between input and output operations. Before a price is read from the English price list, the locale must be switched from the German locale used for printing the invoice to a US English locale. Before inserting the price into the invoice, the global locale must be switched back to the German locale. To read the next input from the price list, the locale must be switched back to English, and so forth. Figure 6 summarizes this activity.</P>

<P><I>Figure 6 � Multiple locales in C</I></P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0></P>

<P>Here is the C code that corresponds to the previous example:</P>

<P>double price;</P>

<P>char buf[SZ];</P>

<P>while ( ... )  // processing the German invoice</P>

<P>{  setlocale(LC_ALL, "En_US");</P>

<P>fscanf(priceFile,"%lf",&amp;price);</P>

<P>// convert $ to DM according to the current exchange rate</P>

<P>setlocale(LC_ALL,"De_DE");</P>

<P>strfmon(buf,SZ,"%n",price);</P>

<P>fprintf(invoiceFile,"%s",buf);</P>

<P>}</P>

<P>Using C++ locale objects dramatically simplifies the task of communicating between multiple locales. The iostreams in the Standard C++ Library are internationalized so that streams can be imbued with separate locale objects. For example, the input stream can be imbued with an English locale object, and the output stream can be imbued with a German locale object. In this way, switching locales becomes unnecessary, as demonstrated in Figure 7:</P>

<P><I>Figure 7 � Multiple locales in C++</I></P>

<P><IMG SRC="../images/bm8.gif.html" ALT="" BORDER=0></P>

<P>The C++ code corresponding to the previous example is provided below. </P>

<P>[<B>Note</B>: This example assumes that you have created a class, <I>moneytype</I>, to represent monetary values, and that you have written iostreams insertion &lt;&lt; and extraction &gt;&gt; operators for the class. Further, it assumes that these operators format and parse values using the money_put and money_get facets of the locales imbued on the streams they�re operating on. An <A HREF="bcb63vjn.htm">example</A> of this technique is provided, using phone numbers rather than monetary values. The <I>moneytype</I> class is not part of the Standard C++ Library.]</P>

<P>priceFile.imbue(locale("En_US"));</P>

<P>invoiceFile.imbue(locale("De_DE"));</P>

<P>moneytype price;</P>

<P>while ( ... )  // processing the German invoice</P>

<P>{  priceFile &gt;&gt; price;</P>

<P>// convert $ to DM according to the current exchange rate</P>

<P>invoiceFile &lt;&lt; price;</P>

<P>}</P>

<P>Because the examples given above are brief, switching locales might look like a minor inconvenience. However, it is a major problem once code conversions are involved.</P>

<P>To underscore the point, let us revisit the JIS encoding scheme using the shift sequence described in Figure 2, which is repeated for convenience here as Figure 8. With these encodings, you will recall that you must maintain a <I>shift state </I>while parsing a character sequence:</P>

<P><I>Figure 8 � The Japanese text encoded in JIS from Figure 2</I></P>

<P><IMG SRC="../images/bm9.gif.html" ALT="" BORDER=0></P>

<P>Suppose you are parsing input from a multibyte file which contains text that is encoded in JIS, as shown in Figure 9. While you parse this file, you have to keep track of the current shift state so you know how to interpret the characters you read, and how to transform them into the appropriate internal wide character representation.</P>

<P><I>Figure 9 � Parsing input from a multibyte file using the global C locale</I></P>

<P><IMG SRC="../images/bm10.gif.html" ALT="" BORDER=0></P>

<P>The global C locale can be switched during parsing; for example, from a locale object specifying the input to be in JIS encoding, to a locale object using EUC encoding instead. The current shift state becomes invalid each time the locale is switched, and you have to carefully maintain the shift state in an application that switches locales.</P>

<P>As long as the locale switches are intentional, this problem can presumably be solved. However, in multithreaded environments, the global C locale may impose a severe problem, as it can be switched inadvertently by another otherwise unrelated thread of execution. For this reason, internationalizing a C program for a multithreaded environment is difficult.</P>

<P>If you use C++ locales, on the other hand, the problem simply goes away. You can imbue each stream with a separate locale object, making inadvertent switches impossible. Let us now see how C++ locales are intended to be used.</P>

</BODY>
</HTML>
