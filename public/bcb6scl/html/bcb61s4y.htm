<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>allocator</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0> Standard C++ Library: Class Reference</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="allocator">
</OBJECT>

<H1><A NAME="allocator"></A>allocator</H1>

<P><B>Summary</B></P>

<P>The default allocator object for storage management in Standard Library containers.</P>

<P><B>Synopsis</B></P>

<P>#include &lt;memory&gt;</P>

<P>template &lt;class T&gt;</P>

<P>class allocator;</P>

<P><B>Description</B></P>

<P>Containers in the Standard Library allow you control of storage management through the use of allocator objects. Each container has an allocator template parameter specifying the type of allocator to be used. Every constructor, except the copy constructor, has an allocator parameter, allowing you to pass in a specific allocator. A container uses that allocator for all storage management. </P>

<P>The library has a default allocator, called allocator. This allocator uses the global new and delete operators. By default, all containers use this allocator. You can also design your own allocator, but if you do so it must have an appropriate interface. The standard interface and an alternate interface are specified below. The alternate interface works on all supported compilers.</P>

<P><B>The Alternate Allocator</B></P>

<P>As of this writing, very few compilers support the full range of features needed by the standard allocator. If your compiler does not support member templates, both classes and functions, then you must use the alternate allocator interface. This alternate interface requires no special features of a compiler and offers most of the functionality of the standard allocator interface. The only thing missing is the ability to use special pointer and reference types. The alternate allocator fixes these as T* and T&amp;. If your compiler supports partial specialization, then even this restriction is removed.</P>

<P>From outside a container, use of the alternate allocator is transparent. Simply pass the allocator as a template or function parameter exactly as you would pass the standard allocator.</P>

<P>Within a container, the alternate allocator interface is more complicated to use because it requires two separate classes, rather than one class with another class nested inside. If you plan to write your own containers and need to use the alternate allocator interface, we recommend that you support the default interface as well, since that is the only way to ensure long-term portability. See the User's Guide section on building containers for an explanation of how to support both the standard and the alternate allocator interfaces.</P>

<P>A generic allocator must be able to allocate space for objects of arbitrary type, and it must be able to construct those objects on that space. For this reason, the allocator must be type aware, but it must be aware on any arbitrary number of different types, since there is no way to predict the storage needs of any given container.</P>

<P>Consider an ordinary template. Although you may be able to instantiate on any fixed number of types, the resulting object is aware of only those types and any other types that can be built up from them (T*, for instance), as well as any types you specify up front. This won't work for an allocator, because you can't make any assumptions about the types a container needs to construct. It may well need to construct Ts (or it may not), but it may also need to allocate node objects and other data structures necessary to manage the contents of the container. Clearly there is no way to predict what an arbitrary container might need to construct. As with everything else within the Standard Library, it is absolutely essential to be fully generic.</P>

<P>The Standard allocator interface solves the problem with member templates. The precise type you are going to construct is not specified when you create an allocator, but when you actually go to allocate space or construct an object on existing space.</P>

<P>The alternate allocator interface uses a different technique. The alternate interface breaks the allocator into two pieces: an interface and an implementation. The interface is a template class containing a pointer to an implementation. The implementation is a simple class providing raw un-typed storage. Anything can be constructed on it. The interface template types the raw storage based on the template parameter. Only the implementation object is passed into a container. The container constructs interface objects as necessary, using the implementation to manage the storage of data.</P>

<P>Since all interface objects use the one copy of the implementation object to allocate space, that one implementation object manages all storage acquisition for the container. The container makes calls to the <B><I>allocator_interface</I></B> objects in the same way it would make calls to a standard allocator object.</P>

<P>For example, if your container needs to allocate T objects and node objects, you need to have two <B><I>allocator_interface</I></B> objects in your container:</P>

<P>allocator_interface&lt;Allocator,T&gt; value_allocator; allocator_interface&lt;Allocator,node&gt; node_allocator;</P>

<P>You then use the value_allocator for all allocation, construction, etc. of values (Ts), and use the node_allocator object to allocate and deallocate nodes.</P>

<P>The only significant drawback is the lack of special pointer types and the inability to alter the behavior of the construct and destroy functions, since these must reside in the interface class. If your compiler has partial specialization, then this restriction goes away, since you can provide specialized interfaces along with your implementation.</P>

<P><B>Standard Interface</B></P>

<P>template &lt;class T&gt;</P>

<P>class allocator {</P>

<P>typedef size_t            size_type;</P>

<P>typedef ptrdiff_t         difference_type;</P>

<P>typedef T*                pointer;</P>

<P>typedef const T*          const_pointer;</P>

<P>typedef T&amp;                reference;</P>

<P>typedef const T&amp;          const_reference;</P>

<P>typedef T                 value_type;</P>

<P>template &lt;class U&gt; struct rebind {</P>

<P>typedef allocator&lt;U&gt; other;</P>

<P>};</P>

<P>allocator () throw();</P>

<P>allocator (const allocator&amp;) throw ();</P>

<P>template &lt;class U&gt; allocator(const allocator&lt;U&gt;&amp;) throw();</P>

<P>template &lt;class U&gt; </P>

<P>allocator&amp; operator=(const allocator&lt;U&gt;&amp;) throw();</P>

<P>~allocator () throw();</P>

<P>pointer address (reference) const;</P>

<P>const_pointer address (const_reference) const;</P>

<P>pointer allocate (size_type,</P>

<P>typename allocator&lt;void::const_pointer = 0);</P>

<P>void deallocate(pointer p, size_type n); </P>

<P>size_type max_size () const;</P>

<P>void construct (pointer, const T&amp;);</P>

<P>void destroy (pointer);</P>

<P>};</P>

<P>// specialize for void:</P>

<P>template &lt;&gt; class allocator&lt;void&gt; {</P>

<P>public:</P>

<P>typedef void*       pointer;</P>

<P>typedef const void* const_pointer;</P>

<P>//  reference-to-void members are impossible.</P>

<P>typedef void value_type;</P>

<P>template &lt;class U&gt; </P>

<P>struct rebind { typedef allocator&lt;U&gt; other; };</P>

<P>};</P>

<P>// globals</P>

<P>template &lt;class T, class U&gt;</P>

<P>bool operator==(const allocator&lt;T&gt;&amp;, </P>

<P>const allocator&lt;U&gt;&amp;) throw();</P>

<P>template &lt;class T, class U&gt;</P>

<P>bool operator!=(const allocator&lt;T&gt;&amp;, </P>

<P>const allocator&lt;U&gt;&amp;) throw();</P>

<P><B>Types</B></P>

<P>size_type</P>

<P>Type used to hold the size of an allocated block of storage.</P>

<P>difference_type</P>

<P>Type used to hold values representing distances between storage addresses.</P>

<P>pointer</P>

<P>Type of pointer returned by allocator.</P>

<P>const_pointer</P>

<P>Const version of pointer.</P>

<P>reference</P>

<P>Type of reference to allocated objects.</P>

<P>const_reference</P>

<P>Const version of reference.</P>

<P>value_type</P>

<P>Type of allocated object.</P>

<P>template &lt;class U&gt; struct rebind;</P>

<P>Converts an allocator templatized on one type to an allocator templatized on another type. This struct contains a single type member: </P>

<P>typedef allocator&lt;U&gt; other</P>

<P><B>Constructors</B></P>

<P><B>allocator</B>()</P>

<P>Default constructor.</P>

<P>template &lt;class U&gt; </P>

<P><B>allocator</B>(const allocator&lt;U&gt;&amp;) </P>

<P>Copy constructor.</P>

<P><B>Destructors</B></P>

<P><B>~allocator</B>()</P>

<P>Destructor.</P>

<P><B>Member Functions</B></P>

<P>pointer</P>

<P><B>address</B>(reference x) const;</P>

<P>Returns the address of the reference x as a pointer.</P>

<P>const_pointer </P>

<P><B>address</B>(const_reference x) const;</P>

<P>Returns the address of the reference x as a const_pointer.</P>

<P>pointer </P>

<P><B>allocate</B>(size_type n, </P>

<P>typename allocator&lt;void&gt;::const_pointer p = 0)</P>

<P>Allocates storage. Returns a pointer to the first element in a block of storage n*sizeof(T) bytes in size. The block is aligned appropriately for objects of type T. Throws the exception bad_alloc if the storage is unavailable. This function uses operator new(size_t). The second parameter p can be used by an allocator to localize memory allocation, but the default allocator does not use it.</P>

<P>void </P>

<P><B>deallocate</B>(pointer p, size_type n)</P>

<P>Deallocates the storage obtained by a call to allocate with arguments n and p.</P>

<P>size_type </P>

<P><B>max_size</B>() const;</P>

<P>Returns the largest size for which a call to allocate might succeed.</P>

<P>void </P>

<P><B>construct</B>(pointer p, const T&amp; val);</P>

<P>Constructs an object of type T2 with the initial value of val at the location specified by p. This function calls the placement new operator.</P>

<P>void </P>

<P><B>destroy</B>(pointer p)</P>

<P>Calls the destructor on the object pointed to by p, but does not delete.</P>

<P><B>Alternate Interface </B></P>

<P>class allocator </P>

<P>{ </P>

<P>public: </P>

<P>typedef size_t               size_type ; </P>

<P>typedef ptrdiff_t            difference_type ;</P>

<P>allocator (); </P>

<P>~allocator ();</P>

<P>void * allocate (size_type, void * = 0); </P>

<P>void deallocate (void*); </P>

<P>};</P>

<P>template &lt;class Allocator,class T&gt; </P>

<P>class allocator_interface .</P>

<P>{ </P>

<P>public: </P>

<P>typedef Allocator        allocator_type ; </P>

<P>typedef T*               pointer ; .</P>

<P>typedef const T*         const_pointer ;    </P>

<P>typedef T&amp;               reference ; .</P>

<P>typedef const T&amp;         const_reference ; </P>

<P>typedef T                value_type ; .</P>

<P>typedef typename Allocator::size_type    size_type ; </P>

<P>typedef typename Allocator::size_type    difference_type ; </P>

<P>protected:</P>

<P>allocator_type*     alloc_;</P>

<P>public: </P>

<P>allocator_interface (); </P>

<P>allocator_interface (Allocator*);</P>

<P>pointer address (T&amp; x); </P>

<P>size_type max_size () const; </P>

<P>pointer allocate (size_type, pointer = 0); </P>

<P>void deallocate (pointer); </P>

<P>void construct (pointer, const T&amp;); </P>

<P>void destroy (T*);</P>

<P>};</P>

<P>//</P>

<P>// Specialization </P>

<P>//</P>

<P>class allocator_interface &lt;allocator,void&gt; </P>

<P>{ </P>

<P>typedef void*                 pointer ; </P>

<P>typedef const void*           const_pointer ;</P>

<P>};</P>

<P><B>Alternate Allocator Description</B></P>

<P>The description for the operations of <B><I>allocator_interface&lt;T&gt;</I></B> are generally the same as for corresponding operations of the standard allocator. The exception is that <B><I>allocator_interface</I></B> members allocate and deallocate call respective functions in <B><I>allocator</I></B>, which are in turn implemented like the standard allocator functions.</P>

<P>See the <B><I>container</I></B> section of the Class Reference for a further description of how to use the alternate allocator within a user-defined container.</P>

<P><B>See Also</B></P>

<P>Containers</P>

</BODY>
</HTML>
