<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Copying a stream's data members</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0> Standard C++ Library: Locales and Iostreams Guide</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="streams,copying data members">
</OBJECT>

<H1><A NAME="copying a streams data members"></A>Copying a stream's data members</H1>

<P>To achieve the equivalent effect of a copy, you might consider copying each data member individually. This can be done as follows:</P>

<P>int main(int argc, char argv[])</P>

<P>{</P>

<P>ofstream out;</P>

<P>if (argc &gt; 1)</P>

<P>out.open(argv[1]);</P>

<P>else</P>

<P>{  out.copyfmt(cout);                                       //1</P>

<P>out.clear(cout.rdstate());                               //2</P>

<P>out.rdbuf(cout.rdbuf());                                 //3</P>

<P>} </P>

<P>// output to out, e.g. </P>

<P>out &lt;&lt; "Hello world!" &lt;&lt; endl;</P>

<P>}</P>

<P>//1The copyfmt() function copies all data from the standard output stream cout to the output file stream out, except the error state and the stream buffer. There is a function exceptions() that allows you to copy the exception mask separately, as in cout.exceptions(fil.exceptions()), but you need not do this explicitly, since copyfmt() already copies the exception mask.</P>

<P>//2Here the error state is copied.</P>

<P>//3Here the stream buffer pointer is copied. </P>

<P>Please note the little snag here. After the call to rdbuf(), the buffer is shared between the two streams, as shown in Figure 31:</P>

<P><I>Figure 31 � Copying a stream's internal data results in a shared buffer</I></P>

<P><IMG SRC="../images/bm30.gif.html" ALT="" BORDER=0></P>

<P>13.2.2<B>Sharing Stream Buffers Inadvertently</B></P>

<P>Whether or not you intend to share a stream buffer among streams depends on your application. In any case, it is important that you realize the stream buffer is shared after a call to rdbuf(); in other words, you must monitor the lifetime of the stream buffer object and make sure it exceeds the lifetime of the stream. In our little example above, we use the standard output stream's buffer. Since the standard streams are static objects, their stream buffers have longer lifetimes that most other objects, so we are safe. However, whenever you share a stream buffer among other stream objects, you must carefully consider the stream buffer's lifetime.</P>

<P>The example above has another disadvantage we haven't considered yet, as shown in the following code:</P>

<P>int main(int argc, char argv[])</P>

<P>{</P>

<P>ofstream out;</P>

<P>if (argc &gt; 1)</P>

<P>out.open(argv[1]);</P>

<P>else</P>

<P>{  out.copyfmt(cout);                                       //1</P>

<P>out.clear(cout.rdstate());                               //2</P>

<P>out.rdbuf(cout.rdbuf());                                 //3</P>

<P>} </P>

<P>// output to out, e.g. </P>

<P>out &lt;&lt; "Hello world!" &lt;&lt; endl;</P>

<P>}</P>

<P>//1Copy the values of member variables (other than the streambuffer and the iostate) in cout to out.</P>

<P>//2Set state flags for out to the current state of cout.</P>

<P>//3Replace out�s streambuffer with cout�s streambuffer.</P>

<P>As we copy the standard output stream's entire internal data, we also copy its special behavior. For instance, the standard output stream is synchronized with the standard input stream. (See the series of topics beginning with <A HREF="bcb67icz.htm">Sharing Files Among Streams</A> for further details.) If our output file stream out is a copy of <I>cout</I>, it is forced to synchronize its output operations with all input operations from <I>cin</I>. This might not be desired, especially since synchronization is a time-consuming activity. Here is a more efficient approach using only the stream buffer of the standard output stream:</P>

<P>int main(int argc, char argv[])</P>

<P>{</P>

<P>filebuf* fb = new filebuf;                                  //1</P>

<P>ostream out((argc&gt;1)?                                       //2</P>

<P>fb-&gt;open(argv[1],ios_base::out|ios_base::trunc):          //3</P>

<P>cout.rdbuf());                                            //4</P>

<P>if (out.rdbuf() != fb)</P>

<P>delete fb;</P>

<P>out &lt;&lt; "Hello world!" &lt;&lt; endl;</P>

<P>}</P>

<P>//1Instead of creating a file stream object, which already contains a file buffer object, we construct a separate file buffer object on the heap that we can hand over to an output stream object if needed. This way we can delete the file buffer object if not needed. In the original example, we constructed a file stream object with no chance of eliminating the file buffer object if not used. </P>

<P>//2An output stream is constructed. The stream has either the standard output stream's buffer, or a file buffer connected to a file.</P>

<P>//3If the program is provided with a file name, the file is opened and connected to the file buffer object. (Note that you must ensure that the lifetime of this stream buffer object exceeds the lifetime of the output stream that uses it.) The open() function returns a pointer to the file buffer object. This pointer is used to construct the output stream object.</P>

<P>//4If no file name is provided, the standard output stream's buffer is used.</P>

<P>As in the original example, out inserts through the standard output stream's buffer, but lacks the special properties of a standard stream. </P>

<P>Here is an alternative solution that uses file descriptors, a nonstandard feature of Rogue Wave's implementation of the standard iostreams. [Note: This feature was available in the traditional iostreams, but is not offered by the standard iostreams. Rogue Wave's implementation of the standard iostreams retains the old feature for backward compatibility with the traditional iostreams, but it is a nonstandard feature. Using it might make your application non-portable to other standard iostream libraries.]</P>

<P>int main(int argc, char argv[])</P>

<P>{</P>

<P>ofstream out;</P>

<P>if (argc &gt; 1)     out.open(argv[1]);                        //1</P>

<P>else              out.rdbuf()-&gt;open(1);                     //2</P>

<P>out &lt;&lt; "Hello world!" &lt;&lt; endl;</P>

<P>}</P>

<P>//1If the program is provided with a file name, the file is opened and connected to the file buffer object.</P>

<P>//2Otherwise, the output stream's file buffer is connected to the standard input stream stdout whose file descriptor is 1.</P>

<P>The effect is the same as in the previous solution, because the standard output stream cout is connected to the C standard input file stdout. This is the simplest of all solutions, because it doesn�t involve reassigning or sharing stream buffers. The output file stream's buffer is simply connected to the right file. However, this is a nonstandard solution, and may decrease portability.</P>

</BODY>
</HTML>
