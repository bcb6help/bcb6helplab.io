<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Overview: Ordered collection algorithms</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm0.gif.html" ALT="" BORDER=0> Standard C++ Library: User's Guide</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="algorithms,ordered collection">
</OBJECT>

<H1><A NAME="overview ordered collection algorithms"></A>Overview: Ordered collection algorithms</H1>

<P>In this section we describe the generic algorithms in the Standard C++ Library that are specific to ordered collections. These algorithms are summarized in the following table:</P>

<TABLE cols=3 width=479>

<TR VALIGN="top">
<TD width=42%><B>Algorithm</B></TD>
<TD colspan=2 width=58%><B>Purpose</B></TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%><B>Sorting algorithms</B></TD>
</TR>

<TR VALIGN="top">
<TD width=42%>sort()</TD>
<TD colspan=2 width=58%>Rearranges sequence, places in order</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>stable_sort()</TD>
<TD colspan=2 width=58%>Sorts, retaining original order of equal elements</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>partial_sort()</TD>
<TD colspan=2 width=58%>Sorts only part of sequence</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>partial_sort_copy()</TD>
<TD colspan=2 width=58%>Partial sorts into copy</TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%><B>Nth largest element algorithm</B></TD>
</TR>

<TR VALIGN="top">
<TD width=42%>nth_element()</TD>
<TD colspan=2 width=58%>Locates nth largest element</TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%><B>Binary search algorithms</B></TD>
</TR>

<TR VALIGN="top">
<TD width=42%>binary_search()</TD>
<TD colspan=2 width=58%>Searches, returning boolean</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>lower_bound()</TD>
<TD colspan=2 width=58%>Searches, returning first position</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>upper_bound()</TD>
<TD colspan=2 width=58%>Searches, returning last position</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>equal_range()</TD>
<TD colspan=2 width=58%>Searches, returning both positions</TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%><B>Merge ordered sequences algorithm</B></TD>
</TR>

<TR VALIGN="top">
<TD width=42%>merge()</TD>
<TD colspan=2 width=58%>Combines two ordered sequences</TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%>Set operations algoithms</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>set_union()</TD>
<TD colspan=2 width=58%>Forms union of two sets</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>set_intersection()</TD>
<TD colspan=2 width=58%>Forms intersection of two sets</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>set_difference()</TD>
<TD colspan=2 width=58%>Forms difference of two sets</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>set_symmetric_difference()</TD>
<TD colspan=2 width=58%>Forms symmetric difference of two sets</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>includes()</TD>
<TD colspan=2 width=58%>Sees if one set is a subset of another</TD>
</TR>

<TR VALIGN="top">
<TD colspan=2 width=97%><B>Heap operations algorithms</B></TD>
</TR>

<TR VALIGN="top">
<TD width=42%>make_heap()</TD>
<TD colspan=2 width=58%>Turns a sequence into a heap</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>push_heap()</TD>
<TD colspan=2 width=58%>Adds a new value to the heap</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>pop_heap()</TD>
<TD colspan=2 width=58%>Removes largest value from the heap</TD>
</TR>

<TR VALIGN="top">
<TD width=42%>sort_heap()</TD>
<TD colspan=2 width=58%>Turns heap into sorted collection</TD>
</TR>
</TABLE><BR>

<P>Ordered collections can be created using the Standard C++ Library in a variety of ways. For example:</P>

<P><IMG SRC="../images/bm1.gif.html" ALT="" BORDER=0>The containers <B><I>set</I></B>, <B><I>multiset</I></B>, <B><I>map</I></B>, and <B><I>multimap</I></B> are ordered collections by definition.</P>

<P><IMG SRC="../images/bm1.gif.html" ALT="" BORDER=0>A <B><I>list</I></B> can be ordered by invoking the sort() member function.</P>

<P><IMG SRC="../images/bm1.gif.html" ALT="" BORDER=0>A <B><I>vector</I></B>, <B><I>deque</I></B>, or ordinary C++ array can be ordered by using one of the <A HREF="bcb609f7.htm">sorting algorithms</A>.</P>

<P>Like the <A HREF="bcb673g3.htm">generic algorithms</A>, the algorithms described here are not specific to any particular container class. This means that they can be used with a wide variety of types. However, many of them do require the use of random-access <B><I>iterator</I></B>s. For this reason they are most easily used with <B><I>vector</I></B>s, <B><I>deque</I></B>s, or ordinary arrays.</P>

<P>Almost all the algorithms described in this section have two versions. The first version uses the less than operator &lt; for comparisons appropriate to the container element type. The second, and more general, version uses an explicit comparison function object, which we will write as Compare. This function object must be a binary predicate (see <A HREF="bcb67oz7.htm">predicates</A>). Since this argument is optional, we will write it within square brackets in the description of the argument types.</P>

<P>A sequence is considered <I>ordered</I> if for every valid or <I>denotable</I> iterator i with a denotable successor j, the comparison Compare(*j, *i) is false. Note that this does not necessarily imply that Compare(*i, *j) is true. It is assumed that the relation imposed by Compare is transitive, and induces a total ordering on the values.</P>

<P>In the descriptions that follow, two values x and y are said to be equivalent if both Compare(x, y) and Compare(y, x) are false. Note that this need not imply that x == y.</P>

<P><B>Include files</B></P>

<P>As with the <A HREF="bcb673g3.htm">generic algorithms</A>, before you can use any of these algorithms in a program you must include the algorithm header file:</P>

<P># include &lt;algorithm&gt;</P>

</BODY>
</HTML>
