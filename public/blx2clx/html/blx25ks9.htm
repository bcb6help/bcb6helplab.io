<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>TDataSet::MoveBy</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm1.gif.html" ALT="" BORDER=0> CLX Reference</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="MoveBy,">
	<PARAM NAME="Keyword" VALUE="MoveBy,TDataSet">
	<PARAM NAME="Keyword" VALUE="TDataSet,MoveBy">
</OBJECT>

<P><A NAME="tdataset_moveby"></A><B>TDataSet::MoveBy</B></P>

<P><A HREF="blx26wqc.htm">TDataSet</A>See alsoExample</P>

<P>Moves to another record relative to the active record in the dataset.</P>

<P><B>int __fastcall</B> MoveBy(<B>int</B> Distance);</P>

<P><B>Description</B></P>

<P>Call MoveBy to move the active record by the number of records specified by Distance. A positive value for Distance indicates forward progress through the dataset, while a negative value indicates backward progress. For example, the following statement moves backward through the dataset by 10 records:</P>

<P>DataSet1-&gt;MoveBy(-10);</P>

<P>MoveBy posts any changes to the active record and</P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0>Sets the Bof and Eof properties to <B>false</B>.</P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0>If Distance is positive, repeatedly fetches Distance subsequent records (if possible), and makes the last record fetched active. If an attempt is made to move past the end of the file, MoveBy sets Eof to <B>true</B>.</P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0>If Distance is negative, repeatedly fetches the appropriate number of previous records (if possible), and makes the last record fetched active. If an attempt is made to move past the start of the file, MoveBy sets Bof to <B>true</B>. If the dataset is unidirectional, the dataset raises an EDatabaseError exception when MoveBy tries to fetch a prior record.</P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0>Broadcasts information about the record change so that data-aware controls and linked datasets can update.</P>

<P><IMG SRC="../images/bm7.gif.html" ALT="" BORDER=0>Returns the number of records moved. In most cases, Result is the absolute value of Distance, but if MoveBy encounters the beginning-of-file or end-of-file before moving Distance records, Result will be less than the absolute value of Distance.</P>

</BODY>
</HTML>
