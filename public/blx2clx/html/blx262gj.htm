<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>TCustomDBGrid::Options</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<P><B><IMG SRC="../images/bm1.gif.html" ALT="" BORDER=0> CLX Reference</B></P>

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="dgAlwaysShowEditor constant">
	<PARAM NAME="Keyword" VALUE="dgAlwaysShowSelection constant">
	<PARAM NAME="Keyword" VALUE="dgCancelOnExit constant">
	<PARAM NAME="Keyword" VALUE="dgColLines constant">
	<PARAM NAME="Keyword" VALUE="dgColumnResize constant">
	<PARAM NAME="Keyword" VALUE="dgConfirmDelete constant">
	<PARAM NAME="Keyword" VALUE="dgEditing constant">
	<PARAM NAME="Keyword" VALUE="dgIndicator constant">
	<PARAM NAME="Keyword" VALUE="dgMultiSelect constant">
	<PARAM NAME="Keyword" VALUE="dgRowLines constant">
	<PARAM NAME="Keyword" VALUE="dgRowSelect constant">
	<PARAM NAME="Keyword" VALUE="dgTabs constant">
	<PARAM NAME="Keyword" VALUE="dgTitles constant">
	<PARAM NAME="Keyword" VALUE="Options,">
	<PARAM NAME="Keyword" VALUE="Options,TCustomDBGrid">
	<PARAM NAME="Keyword" VALUE="TCustomDBGrid,Options">
	<PARAM NAME="Keyword" VALUE="TDBGridOption type">
	<PARAM NAME="Keyword" VALUE="TDBGridOptions type">
</OBJECT>

<P><A NAME="tcustomdbgrid_options"></A><B>TCustomDBGrid::Options</B></P>

<P><A HREF="blx27gv8.htm">TCustomDBGrid</A> See also</P>

<P>Specifies various display and behavioral properties of the data-aware grid.</P>

<P><B>enum</B> TDBGridOption { dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit,dgMultiSelect };</P>

<P><B>typedef</B> Set&lt;TDBGridOption, dgEditing, dgMultiSelect&gt;  TDBGridOptions;</P>

<P>__<B>property</B> TDBGridOptions Options = {<B>read</B>=FOptions, <B>write</B>=SetOptions, <B>default</B>=3325};</P>

<P><B>Description</B></P>

<P>Set Options to include the desired properties for the data-aware grid. Options is a set drawn from the following values:</P>

<TABLE cols=2 width=553>

<TR VALIGN="top">
<TD width=25%><B>Value</B></TD>
<TD width=75%><B>Meaning</B></TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgEditing</TD>
<TD width=75%>The user can edit data using the grid. dgEditing is ignored if Options includes dgRowSelect.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgAlwaysShowEditor</TD>
<TD width=75%> The grid is always in edit mode. That is, the user does not have to press Enter or F2 before editing the contents of a cell. dgAlwaysShowEditor does nothing unless dgEditing is also included in Options. dgAlwaysShowEditor is ignored if Options includes dgRowSelect.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgTitles</TD>
<TD width=75%>Titles appear at the top of the columns in the grid.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgIndicator</TD>
<TD width=75%>A small pointer appears in the first column to indicate which row is current.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgColumnResize</TD>
<TD width=75%>Columns that are bound to fields can be resized or moved.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgColLines</TD>
<TD width=75%>Lines appear between columns in the grid.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgRowLines</TD>
<TD width=75%>Lines appear between the rows of the grid.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgTabs</TD>
<TD width=75%>The user can navigate through the grid using the Tab and Shift+Tab keys.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgRowSelect</TD>
<TD width=75%>The user can select an entire row, as well as selecting individual cells. If Options includes dgRowSelect, dgEditing and dgAlwaysShowEditor are ignored.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgAlwaysShowSelection</TD>
<TD width=75%>The selected cell displays the focus rectangle even when the grid does not have focus.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgConfirmDelete</TD>
<TD width=75%>A message box appears, asking for confirmation, when the user types Ctrl+Delete to delete a row in the grid.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgCancelOnExit</TD>
<TD width=75%>When the user exits the grid from an inserted record to which the user made no modifications, the inserted record is not posted to the dataset. This prevents the inadvertent posting of empty records.</TD>
</TR>

<TR VALIGN="top">
<TD width=25%>dgMultiSelect</TD>
<TD width=75%>More than one row in the grid can be selected at a time.</TD>
</TR>
</TABLE>

</BODY>
</HTML>
