<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">
<TITLE>Debug Inspector</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000">

<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="Debug Inspector">
	<PARAM NAME="Keyword" VALUE="Debug Inspector">
	<PARAM NAME="Keyword" VALUE="debugging, inspecting data">
	<PARAM NAME="Keyword" VALUE="Inspector window">
</OBJECT>

<H2><A NAME="6d4yzm"></A>Debug Inspector</H2>

<P>See also</P>

<P>The number of tabs and the appearance of the data in the Debug Inspector depends on the type of data you inspect. You can inspect the following types of data: arrays, classes, constants, functions, pointers, scalar variables, and interfaces. The Debug Inspector contains three areas:</P>

<P><IMG SRC="../images/bm2.gif.html" ALT="" BORDER=0>The top of the Debug Inspector shows the name, type, and address or memory location of the inspected element, if available. (When inspecting a function call that returns an object, record, set, or array, the debugger displays �In debugger� in place of the temporarily allocated address.)</P>

<P><IMG SRC="../images/bm2.gif.html" ALT="" BORDER=0>The middle pane contains one or more of the following views depending on the type of data you inspect. To change the view, click its tab.</P>

<P>DataShows data names (or class data members) and current values.</P>

<P>MethodsThis view appears only when you inspect a class, or interface and shows the class methods (member functions) and current address locations.</P>

<P>PropertiesThis view displays only when you inspect an Object class with properties (such as a CLX object) and shows the property names and current values. <BR>
<BR>
The inspector does not automatically report the values of all properties because a function called to evaluate certain properties may have side effects that can affect the behavior of the program you are debugging. For example, if you evaluate certain properties before an object is fully constructed or before the object's associated window is created, some of the functions called will actually try to create the window. When your program actually creates the window, the app will likely raise an exception. <BR>
<BR>
Therefore, for a property whose getters are member functions, the Debug Inspector shows only the name of the getter and setter (if the property has a setter). To see the value of the property, click the ? button that appears next to the getter. The debugger will continue to recalculate the value of the property each time the process stops (such as after a step or at a breakpoint). If you click the ? button again, the debugger stops recalculating the value of the property and again will show the getter as the property's value each time the process stops.</P>

<P><IMG SRC="../images/bm2.gif.html" ALT="" BORDER=0>The bottom of the Debug Inspector shows the data type of the item currently selected in the middle pane.</P>

<P>You can sort by Name or by Declaration order (the current behavior and the default). You change sorting order via the context (right-click) menu Sort By item which has two submenu items: 1) Declaration Order and 2) Name. The current sort order is indicated by a bullet point next to the menu item. </P>

<P><A NAME="ekbse_"></A><B>Debug Inspector commands</B></P>

<P>Right-click the Debug Inspector to access the following commands:</P>

<TABLE cols=2 width=584>

<TR VALIGN="top">
<TD width=31%><B>Command</B></TD>
<TD width=69%><B>Description</B></TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Change</TD>
<TD width=69%>Lets you assign a new value to a data item. An ellipsis (�) appears next to an item that can be changed. You can click the ellipsis as an alternative to choosing the change command.
<P><B>Note:</B> This command is only enabled when you can modify the data item being inspected.</P>
</TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Show Inherited</TD>
<TD width=69%>Switches the view in the Data, Methods, and Properties panes between two modes: one that shows all intrinsic and inherited data members or properties of a class, or one that shows only those declared in the class.</TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Show Fully Qualified Names</TD>
<TD width=69%>Shows inherited members using their fully qualified names. </TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Inspect</TD>
<TD width=69%>Opens a new Debug Inspector on the data element you have selected. This is useful for seeing the details of data structures, classes, and arrays.</TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Descend</TD>
<TD width=69%>Same as the Inspect command, except the current Debug Inspector is replaced with the details that you are inspecting (a new Debug Inspector is not opened). To return to a higher level, use the history list.</TD>
</TR>

<TR VALIGN="top">
<TD width=31%>New Expression</TD>
<TD width=69%>Lets you inspect a new expression. </TD>
</TR>

<TR VALIGN="top">
<TD width=31%>Type Cast</TD>
<TD width=69%>Lets you specify a different data type for an item you want to inspect. Type casting is useful if the Debug Inspector contains a symbol for which there is no type information, and when you want to explicitly set the type for untyped pointers.</TD>
</TR>
</TABLE><BR>

<P><B>Multiple process debugging</B></P>

<P>Inspectors are associated with the thread that was active when they were created. When a thread terminates, only the inspectors that were created while the thread was active are destroyed.</P>

</BODY>
</HTML>
